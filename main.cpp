
class pseudo_lock_guard {
  public:
  pseudo_lock_guard()
      : a_(5)
  {
  }

  auto func(int b) -> int { return a_ * b; }

  private:
  int a_;
};

int main()
{
  pseudo_lock_guard a;

  return 0;
}
